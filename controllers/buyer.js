const moment = require('moment-timezone');
const Seller = require('../models/Seller');
const Ebook = require('../models/Ebook');
const Buyer = require('../models/Buyer');

const generator = require('generate-password');
const nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');

const SALT_ROUNDS = 12;
const DATETIME_FORMAT = 'YYYY-MM-DD hh:mm:ss A';

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.GMAIL_USERNAME,
    pass: process.env.GMAIL_PASSWORD,
  },
});

const AUTORESPONDERS = ['getresponse'];

exports.addBuyer = (req, res, next) => {};

// get autoresponder
// get query
// next() to the data saver middleware

exports.getBuyerDataAndSave = async (req, res, next) => {
  // console.log(req.url, originalUrl, query, headers['user-agent']);

  const userAgent = req.headers['user-agent'];
  const autoresponder = AUTORESPONDERS.find((autoresponder) =>
    userAgent.toLowerCase().includes(autoresponder)
  );
  if (!autoresponder) {
    req.error = { message: 'Wrong data 0', error: autoresponder };
    next();
    return;
  }

  const { campaign_name, action, account_login, contact_email } = req.query;

  const isASubscription = action === 'subscribe';
  if (!isASubscription) {
    req.error = { message: 'Wrong data 1', error: isASubscription };
    next();
    return;
  }

  const query = { email: account_login, isDeleted: false };
  const keys = 'id email lists';
  const seller = await Seller.findOne(query, keys).lean().exec();
  if (!seller) {
    req.error = { message: 'Wrong data 2', error: seller };
    next();
    return;
  }

  const buyerList = seller.lists.find(
    (list) =>
      list.list === campaign_name && list.autoresponder === autoresponder
  );
  if (!buyerList) {
    req.error = { message: 'Wrong data 3', error: buyerList };
    next();
    return;
  }

  if (!seller) {
    req.error = { message: 'No seller found.', error: seller };
    next();
    return;
  }

  const ebookQuery = {
    list: campaign_name,
    sellerId: seller._id,
    autoresponder,
  };
  const ebook = await Ebook.findOne(ebookQuery, 'id deviceLimit').lean().exec();

  if (!ebook) {
    req.error = { message: 'No book found.', error: ebook };
    next();
    return;
  }

  const newEbookBought = {
    id: ebook._id,
    deviceLimit: ebook.deviceLimit,
    subscriptionData: req.query,
  };
  let ownedEBooks = [newEbookBought];

  const buyerQuery = { email: contact_email };
  const buyerKeys = 'email eBooks history';
  const oldBuyer = await Buyer.findOne(buyerQuery, buyerKeys);

  if (oldBuyer && oldBuyer.email === contact_email) {
    const oldEBooksBought = oldBuyer.eBooks;
    ownedEBooks = [...ownedEBooks, ...oldEBooksBought];

    const historyUpdate = {
      eBooks: ownedEBooks,
      action: 'UPDATE',
      updatedOn: moment().tz('Asia/Manila').format(DATETIME_FORMAT),
      updatedBy: 1,
    };
    const documentForUpdate = {
      _id: oldBuyer._id,
    };
    const keysForUpdate = {
      eBooks: ownedEBooks,
      history: [...oldBuyer.history, historyUpdate],
    };
    const res = await Buyer.updateOne(documentForUpdate, keysForUpdate);

    if (!res) {
      req.error = { message: 'Cannot update.', error: res };
      next();
      return;
    }

    sendAnEmail(req.query, ownedEBooks, 'update');
    return;
  }

  const password = generator.generate({
    length: 10,
    numbers: true,
    uppercase: false,
    strict: true,
  });

  const buyer = new Buyer({
    email: contact_email,
    password: bcrypt.hashSync(password, SALT_ROUNDS),
    eBooks: [...ownedEBooks],
    devices: [],
    isDeleted: false,
    history: [
      {
        email: contact_email,
        password: bcrypt.hashSync(password, SALT_ROUNDS),
        eBooks: [...ownedEBooks],
        devices: [],
        action: 'INSERT',
        updatedOn: moment().tz('Asia/Manila').format(DATETIME_FORMAT),
        updatedBy: 1,
      },
    ],
  });

  buyer
    .save()
    .then((result) => {
      sendAnEmail(req.query, { originalPassword: password }, 'insert');
    })
    .catch((error) => {
      req.error = { message: 'Error inserting the buyer.', error };
      next();
      return;
    });
};

const sendAnEmail = async (fromAutoresponder, newData, action) => {
  if (action === 'update') {
    try {
      const { account_login, contact_email, contact_name } = fromAutoresponder;
      let info = await transporter.sendMail({
        from: process.env.GMAIL_USERNAME,
        to: contact_email,
        subject: `Book added to your account, ${contact_name}.`,
        text: `Ebook has been added to your account.`,
        html: `
          <span style="margin-bottom: 10px">Book has been added to your account: ${contact_email}.</span><br>
          <span style="margin-bottom: 10px">Log in to the wesbite/web app to read it.</span><br>
          <span style="margin-bottom: 10px">On behalf of ${account_login}, thank you!</span><br>
          <span style="margin-bottom: 10px">Forgot password? Reply to this email to request a new one.</span><br>
          <span>Download or Install our Website/ Web App <strong>(ShelfPH.com)</strong>: <a href="https://shelfph.com" target="_blank">Click here.</span><br>
          <img src="https://brainfoodies.netlify.app/assets/img/appicon.png" style="width: 100px; margin-top: 30px; "/>
          `,
      });
    } catch (error) {
      req.error = { message: 'Cannot send UPDATE email.', error };
      next();
      return;
    }
    return;
  }
  const { originalPassword } = newData;
  const { account_login, contact_email, contact_name } = fromAutoresponder;
  try {
    let info = await transporter.sendMail({
      from: process.env.GMAIL_USERNAME,
      to: contact_email,
      subject: `Your ShelfPH Log In Details, ${contact_name}.`,
      text: `Log in to the app with this Email: ${contact_email} Password: ${originalPassword} to read your book!`,
      html: `
        <span style="margin-bottom: 10px">To read your book, log in to the website/web app with this</span><br>
        Email: <strong>${contact_email}</strong><br>
        Password: <strong>${originalPassword}</strong><br>
        <div style="margin-bottom: 10px"></div>
        <span>On behalf of ${account_login}, thank you!</span><br>
        <span>Download or Install our Website/ Web App <strong>(ShelfPH.com)</strong>: <a href="https://shelfph.com" target="_blank">Click here.</span><br>
        <img src="https://brainfoodies.netlify.app/assets/img/appicon.png" style="width: 100px; margin-top: 30px; "/>
      `,
    });
  } catch (error) {
    req.error = { message: 'Cannot send ADD email.', error };
    next();
    return;
  }
};

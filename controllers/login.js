const Buyer = require('../models/Buyer');
const Ebook = require('../models/Ebook');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const moment = require('moment-timezone');
const { v4: uuidv4 } = require('uuid');

const DATETIME_FORMAT = 'YYYY-MM-DD hh:mm:ss A';

const bufferToString = (book) => {
  book.eBook = book.eBook.toString('utf8');
  book.cover = book.cover.toString('utf8');
  return book;
};

exports.login = async (req, res, next) => {
  try {
    const { email, password, device } = req.body;

    // TODO: check for duplicate email address
    const user = await Buyer.findOne({
      email,
    })
      .lean()
      .exec();

    if (!user) {
      req.error = { message: 'User not found.', error: user };
      next();
      return;
    }

    const passwordsMatched = bcrypt.compareSync(password, user.password);

    if (!passwordsMatched) {
      req.error = { message: 'Wrong password.', error: passwordsMatched };
      next();
      return;
    }

    let deviceToBeStored = {
      devUuid: uuidv4(),
      ...device,
    };

    let allDevices = user.devices
      ? [...user.devices, deviceToBeStored]
      : [deviceToBeStored];

    const keys = '_id eBook cover title deviceLimit';
    let eBooks = await Promise.all(
      user.eBooks.map(async (eBook) => {
        let result = await Ebook.findById(eBook.id, keys).lean().exec();
        return allDevices.length > result.deviceLimit
          ? null
          : bufferToString(result);
      })
    );
    eBooks = eBooks.filter((eBook) => eBook);

    if (!eBooks.length) {
      req.error = {
        message: 'Cannot find ebooks or device limit reached.',
        error: eBooks,
      };
      next();
      return;
    }

    const token = jwt.sign(
      {
        userId: user._id,
        email: user.email,
      },
      process.env.TOKEN,
      { expiresIn: '9999 years' }
    );

    const deviceUpdateInfo = {
      devices: allDevices,
      action: 'UPDATE (login)',
      updatedOn: moment().tz('Asia/Manila').format(DATETIME_FORMAT),
      updatedBy: 1,
    };

    const history = user.history
      ? [...user.history, deviceUpdateInfo]
      : [deviceUpdateInfo];

    const update = await Buyer.updateOne(
      {
        _id: user._id,
      },
      {
        devices: allDevices,
        history,
      }
    );

    res.json({
      message: 'success',
      data: {
        devices: allDevices,
        user: {
          devUuid: deviceToBeStored.devUuid,
          token,
          eBooks,
          email: user.email,
        },
      },
    });
  } catch (error) {
    req.error = { message: 'Cannot log in.', error };
    next();
  }
};

exports.logout = async (req, res, next) => {
  try {
    const { devUuid, email } = req.body;
    const buyer = await Buyer.findOne({ email: email }).lean().exec();
    const loggedDevices = buyer.devices.filter(
      (loggedDevice) => loggedDevice.devUuid !== devUuid
    );

    const update = await Buyer.updateOne(
      {
        _id: buyer._id,
      },
      {
        devices: loggedDevices,
        history: [
          ...buyer.history,
          {
            devices: loggedDevices,
            action: 'UPDATE (logout)',
            updatedOn: moment().tz('Asia/Manila').format(DATETIME_FORMAT),
            updatedBy: 1,
          },
        ],
      }
    );

    res.status(200).json({
      message: 'logged out',
    });
  } catch (error) {
    req.error = { message: 'Cannot log out.', error };
    next();
  }
};

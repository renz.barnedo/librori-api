const Buyer = require('../models/Buyer');

exports.getDevice = async (req, res, next) => {
  try {
    if (!req.body || !req.body.email) {
      req.error = { message: 'Insufficient request data.', error: req.body };
      next();
      return;
    }

    const email = req.body.email;
    const user = await Buyer.findOne({ email }, 'devices').lean().exec();

    if (!user) {
      req.error = { message: 'Cannot find user.', error: user };
      next();
      return;
    }

    res.status(200).json({
      message: 'updated devices list',
      devices: user.devices,
    });
  } catch (error) {
    req.error = { message: 'Cannot get devices.', error };
    next();
    return;
  }
};

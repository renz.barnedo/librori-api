const Ebook = require('../models/Ebook');
const Buyer = require('../models/Buyer');

const bufferToString = (book) => {
  book.eBook = book.eBook.toString('utf8');
  book.cover = book.cover.toString('utf8');
  return book;
};

exports.getEbooks = async (req, res, next) => {
  try {
    const { email, eBooks } = req.body;
    let keys = 'email eBooks devices';
    const user = await Buyer.findOne({ email }, keys).lean().exec();

    if (!user) {
      req.error = { message: 'Cannot find user.', error: user };
      next();
      return;
    }

    keys = '_id eBook cover title deviceLimit';
    const updatedEBooks = await Promise.all(
      eBooks.map(async (eBook) => {
        const result = await Ebook.findById(eBook._id, keys).lean().exec();
        return !eBook.deviceLimit ? eBook : bufferToString(result);
      })
    );

    res.json({
      message: 'updated ebooks of user',
      eBooks: updatedEBooks,
    });
  } catch (error) {
    req.error = { message: 'Cannot get books.', error };
    next();
    return;
  }
};

// TODO: WIP
exports.insertEbook = async (req, res, next) => {
  try {
    const ebook = new Ebook({ ...req.body.insert, eBook: '' });
    const result = await ebook.save();
    console.log('-----------result-----------');
    console.log(result);
  } catch (error) {
    res.status(500).json({
      message: 'error',
    });
  }
};

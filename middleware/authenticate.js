const jwt = require('jsonwebtoken');

exports.authenticateUser = (req, res, next) => {
  const authHeader = req.get('Authorization');
  if (!authHeader) {
    const error = new Error('no authorization');
    error.status = 400;
    throw error;
  }
  const token = authHeader.split(' ')[1];
  let decodedToken;
  try {
    decodedToken = jwt.verify(token, process.env.TOKEN);
  } catch (error) {
    if (error.message === 'jwt expired') {
      error.display = 'Session expired. Login again.';
    }
    error.status = 401;
    throw error;
  }
  if (!decodedToken) {
    const error = new Error('not authenticated.');
    error.status = 422;
    throw error;
  }
  req.userId = decodedToken.userId;
  req.role = decodedToken.role;
  next();
};

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const buyerSchema = new Schema({
  sellerId: {
    type: String,
    required: true,
  },
  autoresponder: {
    type: String,
    required: true,
  },
  list: {
    type: String,
    required: true,
  },
  deviceLimit: {
    type: Number,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  eBook: {
    type: Buffer,
  },
  cover: {
    type: Buffer,
  },
  isDeleted: {
    type: Boolean,
    required: true,
  },
  history: {
    type: Array,
    required: true,
  },
});

module.exports = mongoose.model('Ebook', buyerSchema);

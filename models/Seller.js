const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const sellerSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  isDeleted: {
    type: Boolean,
    required: true,
  },
  history: {
    type: Array,
    required: true,
  },
});

module.exports = mongoose.model('Seller', sellerSchema);

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const buyerSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  eBooks: {
    type: Array,
    required: true,
  },
  devices: {
    type: Array,
  },
  isDeleted: {
    type: Boolean,
    required: true,
  },
  history: {
    type: Array,
    required: true,
  },
});

module.exports = mongoose.model('Buyer', buyerSchema);

const express = require('express');
const morgan = require('morgan');
const compression = require('compression');
const mongoose = require('mongoose');
const cors = require('cors');
const helmet = require('helmet');
const fs = require('fs');
const path = require('path');
const moment = require('moment-timezone');
const os = require('os');

require('dotenv').config();

const buyerRoutes = require('./routes/buyer');
const loginRoutes = require('./routes/login');
const ebookRoutes = require('./routes/ebook');
const deviceRoutes = require('./routes/device');

const logDirectory = 'logs';
const yearDirectory = `${logDirectory}/${moment()
  .tz('Asia/Manila')
  .format('YYYY')}`;
const monthDirectory = `${yearDirectory}/${moment()
  .tz('Asia/Manila')
  .format('MM')}`;
const dayDirectory = `${monthDirectory}/${moment()
  .tz('Asia/Manila')
  .format('DD')}`;

if (!fs.existsSync(logDirectory)) {
  fs.mkdirSync(logDirectory);
}
if (!fs.existsSync(yearDirectory)) {
  fs.mkdirSync(yearDirectory);
}
if (!fs.existsSync(monthDirectory)) {
  fs.mkdirSync(monthDirectory);
}
if (!fs.existsSync(dayDirectory)) {
  fs.mkdirSync(dayDirectory);
}

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, `${dayDirectory}/access.log`),
  { flags: 'a' }
);

const errorLogStream = fs.createWriteStream(
  path.join(__dirname, `${dayDirectory}/error.log`),
  { flags: 'a' }
);
const app = express();

app.use(helmet());
app.use(morgan('combined', { stream: accessLogStream }));
app.use(compression({ level: 9 }));

let corsConfig;

if (process.env.NODE_ENV === 'production') {
  const allowedDomains = [
    'https://shelfph.com',
    'https://www.shelfph.com',
    'https://linked-app.netlify.app', // TEST client app
    'https://brainfoodies.netlify.app', // TODO: remove this later if set na ang main domain
  ];

  corsConfig = {
    origin: (origin, callback) => {
      if (!origin) {
        return callback(null, true);
      }

      if (allowedDomains.indexOf(origin) === -1) {
        const msg = `This site ${origin} does not have an access. Only specific domains are allowed to access it.`;
        errorLogStream;
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    },
  };
}

app.use(cors(corsConfig));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/customer', buyerRoutes);
app.use('/user', loginRoutes);
app.use('/product', ebookRoutes);
app.use('/limit', deviceRoutes);

app.use((req, res) => {
  try {
    let { message = '', data = null } = req.error;
    const response = { message };
    if (data) {
      response.data = data;
    }
    res.status(500).json(response);
    errorLogStream.write(
      JSON.stringify({
        ...req.body,
        ...req.error,
        datetime: moment()
          .tz('Asia/Manila')
          .format('YYYY-MM-DD hh:mm:ss.SSS A'),
      }) + os.EOL,
      (error) => {
        if (error) {
          console.log('Logging writing log:', error);
        }
      }
    );
  } catch (error) {
    console.log('*****Logging error:', error);
    errorLogStream.write(error.toString() + os.EOL, (error) => {
      if (error) {
        console.log('Error Logging writing error log:', error);
      }
    });
  }
});

const PORT = process.env.PORT || 5000;
const MESSAGE = `Server CONNECTED running on ${process.env.NODE_ENV} mode port on ${PORT}`;

mongoose
  .connect(process.env.MONGO_DB_URI)
  .then((result) => {
    app.listen(PORT, console.log(MESSAGE));
  })
  .catch((error) => {
    console.log('*****Mongo connection error:', error);
    errorLogStream.write('MongoDB: ' + error.toString() + os.EOL, (error) => {
      if (error) {
        console.log('*****Mongo writing log error', error);
      }
    });
  });

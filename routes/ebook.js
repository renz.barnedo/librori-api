const express = require('express');
const router = express.Router();

const { authenticateUser } = require('../middleware/authenticate');
const { getEbooks } = require('../controllers/ebook');

router.post('/ebooks', authenticateUser, getEbooks);

module.exports = router;

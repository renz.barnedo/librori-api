const express = require('express');
const router = express.Router();

const { getDevice } = require('../controllers/device');
const { authenticateUser } = require('../middleware/authenticate');

router.post('/devices', authenticateUser, getDevice);

module.exports = router;

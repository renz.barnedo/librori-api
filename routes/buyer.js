const express = require('express');
const router = express.Router();

const { addBuyer, getBuyerDataAndSave } = require('../controllers/buyer');

router.post('/purchase', addBuyer);
router.get('/purchase', getBuyerDataAndSave);

module.exports = router;

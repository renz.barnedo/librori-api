const express = require('express');
const router = express.Router();

const { authenticateUser } = require('../middleware/authenticate');
const { login, logout } = require('../controllers/login');

router.post('/login', login);
router.post('/logout', authenticateUser, logout);

module.exports = router;
